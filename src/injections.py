from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.memory import pm

from memory import coords_pointer


infinite_knives = AllocatingCodeInjection(
    pm.base_address + 0x8CD6C6,
    """
        mov dword [eax + 0x284], 50
        cmp dword [eax + 0x284], 00
    """,
    original_code_length=7,
)


update_coords_pointer = AllocatingCodeInjection(
    pm.base_address + 0x102112,
    f"""
        mov [{coords_pointer}], eax

        movaps xmm0, [eax + 0x30]
        mov eax, [ebp + 0x08]
    """,
    original_code_length=7,
)
