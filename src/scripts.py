from trainerbase.common import regenerate
from trainerbase.scriptengine import ScriptEngine, system_script_engine

from objects import hp, max_hp


@system_script_engine.simple_script
def always_full_hp():
    if hp.value < max_hp.value:
        hp.value = max_hp.value


regeneration_script_engine = ScriptEngine(delay=1)


@regeneration_script_engine.simple_script
def regenerate_hp():
    regenerate(hp, max_hp, percent=10)
