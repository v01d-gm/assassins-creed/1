from dearpygui import dearpygui as dpg
from trainerbase.gui import (
    CodeInjectionUI,
    GameObjectUI,
    ScriptUI,
    SeparatorUI,
    SpeedHackUI,
    TeleportUI,
    add_components,
    simple_trainerbase_menu,
)

from injections import infinite_knives
from objects import hp, max_hp
from scripts import always_full_hp, regenerate_hp
from teleport import tp


@simple_trainerbase_menu("Assassin's Creed™: Director's Cut Edition", 700, 350)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                GameObjectUI(hp, "HP"),
                GameObjectUI(max_hp, "Max HP"),
                ScriptUI(always_full_hp, "Always Full HP", "F1"),
                ScriptUI(regenerate_hp, "Regenerate HP", "F2"),
                CodeInjectionUI(infinite_knives, "Infinite Knives", "F3"),
                SeparatorUI(),
                SpeedHackUI(),
            )

        with dpg.tab(label="Teleport"):
            add_components(TeleportUI(tp, "Insert", "Home", "K"))
