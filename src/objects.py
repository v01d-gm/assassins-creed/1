from trainerbase.gameobject import GameFloat, GameInt

from memory import hp_address, player_coords_address


hp = GameInt(hp_address)
max_hp = GameInt(hp_address.inherit(new_add=4))

player_x = GameFloat(player_coords_address)
player_y = GameFloat(player_coords_address.inherit(new_add=0x4))
player_z = GameFloat(player_coords_address.inherit(new_add=0x8))
