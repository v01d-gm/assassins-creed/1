from trainerbase.common import Teleport

from objects import player_x, player_y, player_z


tp = Teleport(
    player_x,
    player_y,
    player_z,
    {
        "Al Mualim": (208.2404327392578, 11.139894485473633, 107.6415023803711),
        "Damascus | Assassin Bureau": (45.92155838012695, 5.514058589935303, 4.137312412261963),
    },
)
