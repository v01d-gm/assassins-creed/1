from trainerbase.memory import Address, allocate_pointer, pm


hp_address = Address(pm.base_address + 0x159EC00, [0xB78, 0x14])

coords_pointer = allocate_pointer()
player_coords_address = Address(coords_pointer, [0x30])
